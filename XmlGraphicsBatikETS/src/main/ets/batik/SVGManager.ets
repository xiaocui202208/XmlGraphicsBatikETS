/**
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fileio from '@ohos.fileio';
import xml from '@ohos.convertxml';
import featureAbility from '@ohos.ability.featureAbility';
import SVGAttrConstants from "./constants/SVGAttrConstants"
import {SVGDeclares} from "./svggen/SVGDeclares";
import {hasKeyInObj, checkElements} from "./util/ObjOrArrayUtil";
import {consoleInfo} from "./util/LogUtil";
import {SVGXMLChecker} from "./SVGXMLChecker"

/**
 * SVG管理器
 */
export class SVGManager {
  private static _sInstance: SVGManager;
  private _svgObj: Object;
  private _filePath: string= '';
  private _checker: SVGXMLChecker;

  public static getInstance(): SVGManager{
    if (!this._sInstance) {
      this._sInstance = new SVGManager();
    }
    return this._sInstance;
  }

  /**
   * 构造函数
   */
  private constructor() {
    // 实现单例，私有构造方法
    this._svgObj = Object.create(null);
    this._checker = new SVGXMLChecker();
    this.getFilePath((path) => {
      this._filePath = path;
    })
  }

  /**
   * 获取整个SVG的整体对象
   */
  public getSVGTotalObj() {
    return this._svgObj;
  }

  /**
   * 创建SVG的头部声明
   */
  public createSVGDeclares(): Object{
    // 为整个SVG文档添加设置标准SVG声明
    var declares = new SVGDeclares();
    declares.setXMLVersion('1.0');
    declares.setEncoding('utf-8');
    declares.setStandalone(true);
    this._svgObj = declares.toObj();
    return this._svgObj;
  }

  /**
   * 获取SVG标签对应的对象
   *
   * 找到标签内容为svg的对象
   * @param obj svg文件对应的对象
   * @return 返回svg标签对应的JS对象，如果没找到，返回false
   */
  public getSVGRoot(obj: Object = this._svgObj): object{
    var svgObj: object;

    // 获取节点信息对象
    var svgElements = obj[SVGAttrConstants.ATTR_KEY_ELEMENTS];
    if (typeof svgElements !== SVGAttrConstants.TYPEOF_OBJECT || !Array.isArray(svgElements)) {
      return svgObj;
    }
    svgElements.forEach(nodeObj => {
      if (typeof nodeObj !== SVGAttrConstants.TYPEOF_OBJECT) {
        return;
      }
      for (var svgObjKey in nodeObj) {
        if (svgObjKey === SVGAttrConstants.ATTR_KEY_NAME && nodeObj[svgObjKey] === 'svg') {
          svgObj = nodeObj;
          break;
        }
      }
    })
    return svgObj;
  }

  /**
   * 为指定对象添加子节点
   * @param parentObj 要添加子节点的对象
   * @param childPropertyValue 对应主键的值
   */
  public addChildNode(parentObj: Object, childPropertyValue: Object): boolean{
    if (!checkElements(childPropertyValue)) {
      throw Error('node keys must in [`type`, `text`] or [`type`, `name`, `attributes`, `elements`]');
      return false;
    }
    if (hasKeyInObj(parentObj, SVGAttrConstants.ATTR_KEY_ELEMENTS)
    && Array.isArray(parentObj[SVGAttrConstants.ATTR_KEY_ELEMENTS])) {
      parentObj[SVGAttrConstants.ATTR_KEY_ELEMENTS].push(childPropertyValue);
    } else if (!hasKeyInObj(parentObj, SVGAttrConstants.ATTR_KEY_ELEMENTS)) {
      var elementsArray = new Array();
      elementsArray.push(childPropertyValue);
      parentObj[SVGAttrConstants.ATTR_KEY_ELEMENTS] = elementsArray;
    }
    return true;
  }

  /**
   * 为某个节点设置自己点（可能会覆盖原有值）
   * @param parentObj 父节点
   * @param childPropertyValue 子节点
   * @return 返回是否设置添加子节点
   */
  public setChildNode(parentObj: Object, childPropertyValue: Object): boolean{
    if (!checkElements(childPropertyValue)) {
      throw Error('node keys must in [`type`, `text`] or [`type`, `name`, `attributes`, `elements`]');
      return false;
    }

    if (typeof childPropertyValue === SVGAttrConstants.TYPEOF_OBJECT) {
      if (Array.isArray(childPropertyValue)) {
        parentObj[SVGAttrConstants.ATTR_KEY_ELEMENTS] = childPropertyValue;
      } else {
        var elementsArray = new Array();
        elementsArray.push(childPropertyValue);
        parentObj[SVGAttrConstants.ATTR_KEY_ELEMENTS] = elementsArray;
      }
      return true;
    }
    return false;
  }

  /**
   * 保存SVG文件
   * @param fileName svg文件名称：svg.svg
   * @param fileContent SVG文件的路径/内容
   */
  public saveSVG(fileName: string, fileContent: string | Object, onSuccess?: Function, onFailed?: Function): void{
    if (!fileName || fileName.length < 5 || fileName.substr(fileName.length - 4) !== '.svg') {
      onFailed(0, 'the suffix of SVG file must be .svg');
      return;
    }

    var fileXMLInfo = '';
    if (typeof fileContent === 'object' && !Array.isArray(fileContent)) {
      var fileContentText: string = this.convertObjToXML(fileContent);
      fileXMLInfo = fileContentText;
    } else if (typeof fileContent === 'string') {
      fileXMLInfo = fileContent;
    }

    let that = this;
    if (fileXMLInfo) {
      try {
        this._checker.check(fileXMLInfo, function () {
          var newFileName = '';
          var pathArray: string[] = fileName.split('/');
          if (pathArray.length >= 2) {
            var folderArray = pathArray.splice(0, pathArray.length - 1);
            folderArray.forEach((folderName) => {
              if (folderName) {
                newFileName += folderName;
                that.createFolder(that._filePath + '/' + newFileName);
                newFileName += '/';
              }
            })
            newFileName += pathArray.splice(pathArray.length - 1);
          } else {
            newFileName = fileName;
          }
          var svgURI = that._filePath + '/' + newFileName;
          that._createFile(svgURI);
          that._writeFile(svgURI, fileXMLInfo);
        })
      } catch (e) {
        consoleInfo('SVGManager saveFile checker failed ', e)
        onFailed(1, e);
      }
    }
  }

  /**
   * 转换数据为XML格式
   * @param fileContentObj 要被转换为XML格式的对象
   * @return xml格式数据
   */
  public convertObjToXML(fileContentObj: Object): string{
    if (typeof fileContentObj === SVGAttrConstants.TYPEOF_OBJECT) {
      var xmlResult = '';
      for (var element in fileContentObj) {
        if (element === SVGAttrConstants.ATTR_KEY_DECLARATION) {
          xmlResult += this._convertDeclaration(fileContentObj[element]);
        } else if (element === SVGAttrConstants.ATTR_KEY_ELEMENTS) {
          xmlResult += this._convertArrayToXML(fileContentObj[SVGAttrConstants.ATTR_KEY_ELEMENTS]);
        }
      }
      return xmlResult;
    }
    return null;
  }

  /**
   * 转换数组为XML格式
   * @param arrayObj 要被转换为XML格式的数组对象
   * @return xml格式数据
   */
  private _convertArrayToXML(arrayObj: any): string{
    var arrayXml = '';
    if (typeof arrayObj === SVGAttrConstants.TYPEOF_OBJECT && Array.isArray(arrayObj)) {
      arrayObj.forEach(element => {
        arrayXml += this._convertObjToXML(element);
      });
    }
    return arrayXml;
  }

  /**
   * 转换Obj数据为XML格式
   * @param contentObj 要被转换为XML格式的对象
   * @return xml格式数据
   */
  private _convertObjToXML(contentObj: Object): string{
    var objXml = '';
    if (typeof contentObj !== SVGAttrConstants.TYPEOF_OBJECT || !hasKeyInObj(contentObj, SVGAttrConstants.ATTR_KEY_TYPE)) {
      return objXml;
    }

    var elementType = contentObj[SVGAttrConstants.ATTR_KEY_TYPE];
    if (elementType === SVGAttrConstants.ATTR_VALUE_ELEMENT) {
      objXml = '<'
    }

    if (hasKeyInObj(contentObj, SVGAttrConstants.ATTR_KEY_NAME)) {
      objXml += contentObj[SVGAttrConstants.ATTR_KEY_NAME] + " ";
    }

    if (hasKeyInObj(contentObj, SVGAttrConstants.ATTR_KEY_ATTRIBUTES)) {
      var attrObj = contentObj[SVGAttrConstants.ATTR_KEY_ATTRIBUTES];
      if (typeof attrObj !== SVGAttrConstants.TYPEOF_OBJECT) {
        return;
      }
      for (var attrKey in attrObj) {
        if (typeof attrObj[attrKey] === SVGAttrConstants.TYPEOF_OBJECT) {
          var viewBoxObj = attrObj[attrKey];
          objXml += attrKey + "=\"";
          for (var key in viewBoxObj) {
            objXml += viewBoxObj[key] + ' ';
          }
          objXml += "\"";
        } else {
          objXml += this._parseGeneralTypeToXML(attrKey, attrObj[attrKey]);
        }
      }
    }

    if (elementType === SVGAttrConstants.ATTR_VALUE_ELEMENT) {
      if (hasKeyInObj(contentObj, SVGAttrConstants.ATTR_KEY_ELEMENTS)) {
        objXml += '>';
        objXml += this._convertArrayToXML(contentObj[SVGAttrConstants.ATTR_KEY_ELEMENTS]);
        objXml += '</' + contentObj[SVGAttrConstants.ATTR_KEY_NAME] + '>';
      } else {
        objXml += '/>';
      }
    } else if (elementType === SVGAttrConstants.ATTR_KEY_OR_VALUE_TEXT) {
      objXml += contentObj[SVGAttrConstants.ATTR_KEY_OR_VALUE_TEXT];
    }

    return objXml;
  }

  /**
   * 转换声明数据为XML格式
   * @param declarationObj 要被转换为XML格式的对象
   * @return xml格式数据
   */
  private _convertDeclaration(declarationObj: Object): string{
    var declarationString = '<?xml ';
    if (hasKeyInObj(declarationObj, SVGAttrConstants.ATTR_KEY_ATTRIBUTES)) {
      var declarationAttrObj = declarationObj[SVGAttrConstants.ATTR_KEY_ATTRIBUTES];
      for (var element in declarationAttrObj) {
        declarationString += this._parseGeneralTypeToXML(element, declarationAttrObj[element]);
      }
    }
    declarationString += '?>';
    return declarationString;
  }

  /**
   * 转换键值对数据为XML格式
   * @param key 主键
   * @param generalObj 值
   */
  private _parseGeneralTypeToXML(key: string, value: number | string): string {
    var xml = key + "=\"" + value + "\" ";
    return xml;
  }

  /**
   * 获取SVG标签对应的对象/值
   * @param parentObj 整个XML对应的对象
   * @param key  主键
   * @return false 或 SVG根标签对应的对象
   */
  public getValueForKey(parentObj: Object, key: string): any{
    var svgIndex = hasKeyInObj(parentObj, key);
    if (svgIndex) {
      return parentObj[key];
    }
    return false;
  }

  /**
   * 移除子节点/属性
   * @param parentObj 将要删除节点或属性的对象
   * @param key 将要删除的节点或属性对应的key值
   */
  public removeByKey(parentObj: Object, key: string): void {
    var svgIndex = hasKeyInObj(parentObj, key);
    if (svgIndex) {
      delete parentObj[key];
    }
  }

  /**
   * 为对象设置属性或子节点（覆盖原有键值对）
   * @param parentObj 要设置属性或结点的对象
   * @param key 主键
   * @param value 属性/节点
   */
  public setAttribute(parentObj: Object, key: string, value: string): void{
    if (typeof parentObj === SVGAttrConstants.TYPEOF_OBJECT && key && value) {
      var attributesObj: Object = '';
      if (hasKeyInObj(parentObj, SVGAttrConstants.ATTR_KEY_ATTRIBUTES)) {
        attributesObj = parentObj[SVGAttrConstants.ATTR_KEY_ATTRIBUTES];
      } else {
        attributesObj = Object.create(null);
      }
      attributesObj[key] = value;
    }
    parentObj[key] = value;
  }

  /**
    * 创建文件夹
    * @param 文件夹绝对路径
    */
  public createFolder(path: string): void {
    //创建文件夹
    if (!this._isFolderExisted(path)) {
      fileio.mkdirSync(path);
    }
  }

  /**
   * 判断文件夹是否存在
   * @param 文件夹绝对路径
   */
  public _isFolderExisted(path: string): boolean{
    try {
      let stat = fileio.statSync(path)
      return stat.isDirectory()
    } catch (e) {
      consoleInfo('SVGManager existFolder', e.message);
      return false
    }
  }

  /**
   * 新建文件
   * @param path 文件绝对路径及文件名
   */
  private _createFile(path: string): number{
    return fileio.openSync(path, 0o100, 0o666);
  }

  /**
   * 向path写入content数据，覆盖旧数据
   * @param path 文件路径
   * @param content 文件内容
   */
  private _writeFile(path: string, content: ArrayBuffer | string): void{
    try {
      let fd = fileio.openSync(path, 0o102, 0o666);
      fileio.ftruncateSync(fd);
      fileio.writeSync(fd, content);
      fileio.fsyncSync(fd);
      fileio.closeSync(fd);
    } catch (e) {
      consoleInfo('SVGManager writeFile ', 'Failed to writeFile for ' + e);
    }
  }

  /**
   * 获取data路径
   * @param onSuccess 路径数据回调
   */
  public getFilePath(onSuccess: Function): void {
    featureAbility.getContext()
      .getFilesDir()
      .then((data) => {
        onSuccess(data);
      })
      .catch((error) => {
        consoleInfo('SVGManager getFilePath ', error.message);
      })
  }

  /**
   * 解析SVG文件
   * @param fileName 文件路径/名称
   * @param onSuccess 解析成功回调
   * @param onFailed 解析失败回调
   */
  public parse(fileName: string, onSuccess: Function, onFailed?: Function): void{
    // File绝对路径不能有`file://`
    var fileCompletePath = this._filePath + '/' + fileName;
    let that = this;
    fileio.readText(fileCompletePath)
      .then(function (str) {
        try {
          that._checker.check(str, function () {
            var xmlInstance = new xml();
            var xmlResult = xmlInstance.convert(str, {
              trim: false,
              declarationKey: 'declaration',
              instructionKey: 'instruction',
              attributesKey: 'attributes',
              textKey: 'text',
              cdataKey: 'cdata',
              doctypeKey: 'doctype',
              commentKey: 'comment',
              parentKey: 'parent',
              typeKey: 'type',
              nameKey: 'name',
              elementsKey: 'elements'
            });
            if (xmlResult) {
              onSuccess(xmlResult.toString());
            } else {
              consoleInfo('SVGManager parse failed ', 'result is empty');
              onFailed('SVGManager parse failed, result is empty');
            }
          })
        } catch (e) {
          consoleInfo('SVGManager parse checker failed ', e)
          onFailed(e);
        }
      }).catch(e => {
      consoleInfo('SVGManager parse failed catch ', e);
      onFailed(e);
    })
  }
}